@setup

    // on va charger le package dotenv pour pouvoir utiliser les variables qui y sont enregistrés.
    require __DIR__.'/vendor/autoload.php';
    $dotenv = new Dotenv\Dotenv(__DIR__);
    
    // répértoire dans lequel le projet va être déployé sur le serveur 
    $path = "/var/www/sandbox/html";
    $path = rtrim($path, '/');

    if ( substr($path, 0, 1) !== '/' ) throw new Exception('Attention : votre chemin de déploiement ne commence pas par un /');

        
    // is localhost is not 127.0.0.1, specify too
    // le/les serveurs sur lesquels on peut déployer la configuration
    // si l'ip local que vous utilisez n'est pas 127.0.0.1, vous pouvez specifier le serveur localhost ici    
    $servers_to  = [
        'sandbox'   => 'user@access-to-ssh.com'
    ];

    // sur quel serveur l'application va être déployé, sera spécifier par ligne de commande
    $deployTo = isset($deployTo) ? $deployTo : '';

    // deduction du serveur sur lequel l'application sera déployé, si pas spécifié, on utilise le premier.
    $server = (!empty($deployTo)) ?  $servers_to[$deployTo] : array_pop(array_reverse($servers_to));
    $activeServer = ['web' => $server];
    
    // depot du projet
    $repo = "git@bitbucket.org:team/repository.git";

    // branche à déployer
    $branch = isset($branch) ? $branch : "master";

    // Nom du release
    $date = ( new DateTime )->format('YmdHis'); //ou 'Y-m-d_H:i:s'
    // release directory
    $release = $path.'/releases/'.$date;

    // dossiers pérsistants entre les releases
    $shared_folders = [
        'storage'   => 'storage',
        'uploads'   => 'public/uploads'
    ];

    // fichiers pérsistants 
    $shared_files = [
        '.htaccess'   => 'public/.htaccess'
    ];

    // nombre de release à garder
    $deploy_keep = (isset($keep) && is_int($keep) && $keep > 1) ? $keep : 3;

    //Dossier uploadables
    $uploadables_folders = [
        'public/assets',
        'public/css',
        'public/js',
        'public/svg'
    ];

    // Fichiers uploadables
    $uploadables_files = [
        'public/mix-manifest.json'
    ];

    // Environnement
    $env = isset($env) ? $env : "preprod";

    // Compiler les assets en local avant de les uploader
    $buildAssets = isset($buildAssets) ? $buildAssets : false;
 
    //Restart queue
    $restartQueue = isset($restartQueue) ? $restartQueue : false;

    //Health Check Configurations
    $healthCheckUrl = isset($healthCheckUrl) ? $healthCheckUrl : "";
    $httpUser = isset($httpUser) ? $httpUser : "";

    // Slack webhook
    $slackWebhook = isset($slackWebhook) ? $slackWebhook : "";
    
@endsetup

@servers(array_merge($activeServer, ['localhost' => '127.0.0.1']))

@task('init', ['on' => 'web'])
    if [ ! -d {{ $path }}/current ]; then
        cd {{ $path }}
        if  git clone {{ $repo }} --branch={{ $branch }} --depth=1 -q {{ $release }} ; then
            echo "Dépot cloné"
            mkdir shared && chgrp www-data shared && chmod -R g+w shared
            mkdir repo && chgrp www-data repo && chmod -R g+w repo
            mkdir temp && chgrp www-data temp && chmod -R g+w temp

            @if(sizeof($shared_folders) > 0)
                @foreach($shared_folders as $current => $shared_folder)
                    mv {{ $release }}/{{$shared_folder}} {{ $path }}/shared/{{$current}}
                    chmod -R 777 {{ $path }}/shared/{{$current}}
                    chgrp www-data {{ $path }}/shared/{{$current}}
                    ln -s {{ $path }}/shared/{{$current}} {{ $release }}/{{$shared_folder}} 
                    chmod -R 777 {{ $release }}/{{$shared_folder}}
                    chgrp www-data {{ $release }}/{{$shared_folder}}
                    echo "shared '{{$shared_folder}}' directory set up"
                @endforeach
            @endif

            @if(sizeof($shared_files) > 0)
                @foreach($shared_files as $current => $shared_file)
                    cp -n {{ $release }}/{{$shared_file}}.dist {{ $path }}/{{$current}}.dist
                    chgrp www-data {{ $path }}/{{$current}}.dist && chmod 755 {{ $path }}/{{$current}}.dist
                    echo "{{$shared_file}} file set up"
                @endforeach
            @endif

            cp -n {{ $release }}/.env.example {{ $path }}/.env
            chgrp www-data {{$path}}/.env && chmod 755 {{ $path }}/.env
            ln -s {{ $path }}/.env {{ $release }}/.env
            echo "Environment file set up"

            rm -rf {{ $release }}
            echo "Deployment path initialisé. vous pouvez exécuter 'envoy run deploy' maintenant."
        else 
            echo "Dépot inaccessible, vérifiez votre configuration"
        fi
    else
        echo "Deployment path already initialised (current symlink exists)!"
    fi
@endtask

@story('deploy')
    deployment_start
    deployment_links
    deployment_composer
    deployment_migrate
    @if(sizeof($uploadables_folders)>0 || sizeof($uploadables_files)>0)
        @if($buildAssets)
            optionnal_build_assets
        @endif
        create_uploads_folder
        uploads_data
        uploads_apply
    @endif
    deployment_finish
    deployment_cache
    @if($restartQueue)
        restart_queue
    @endif
    deployment_cleanup
    health_check
@endstory

@story('rollback')
    deployment_rollback
    health_check
@endstory

@task('deployment_start', ['on' => 'web'])
    cd {{ $path }}
    echo "Deployment ({{ $date }}) started"
        
    if  git clone {{ $repo }} --branch={{ $branch }} --depth=1 -q {{ $release }} ; then
        cd {{ $release }}
        git config core.filemode true
        cd ..
        chgrp www-data {{ $release }}
        chmod g+w {{ $release }}
        echo "Dépot cloné"
    else
        echo "Clonage du dépot échoué"
    fi
@endtask

@task('deployment_links', ['on' => 'web'])
    cd {{ $path }}

    @foreach($shared_folders as $current => $shared_folder)
        rm -rf {{ $release }}/{{$shared_folder}}
        ln -s {{ $path }}/shared/{{$current}} {{ $release }}/{{$shared_folder}}
        echo "Shared directory {{$shared_folder}} set up"
    @endforeach

    @if(sizeof($shared_files) > 0)
        @foreach($shared_files as $current => $shared_file)
            cp -n {{ $path }}/{{$current}}.dist {{$release}}/{{$shared_file}}
        @endforeach
    @endif

    ln -s {{ $path }}/.env {{ $release }}/.env

    echo "Environment file set up"
@endtask

@task('deployment_composer', ['on' => 'web'])
    cd {{ $release }}
    echo "Installing PHP archive of composer ..."

    EXPECTED_SIGNATURE="$(wget -q -O - https://composer.github.io/installer.sig)"
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    ACTUAL_SIGNATURE="$(php -r "echo hash_file('SHA384', 'composer-setup.php');")"
    if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]
    then
        >&2 echo 'ERROR: Invalid installer signature'
        rm composer-setup.php
        exit 1
    fi
    php composer-setup.php --quiet
    rm composer-setup.php

    echo "Installing composer depencencies..."
    @if($env != "production")
        php composer.phar install --no-interaction --prefer-dist  --optimize-autoloader
    @else
        php composer.phar install --no-dev --no-interaction --prefer-dist --optimize-autoloader
    @endif
    echo "Removing php archive ..."
    rm composer.phar
@endtask

@task('deployment_migrate', ['on' => 'web'])
    php {{ $release }}/artisan migrate --env={{ $env }} --force --no-interaction
@endtask

@task('deployment_cache', ['on' => 'web'])
    echo "Clear cache"
    php {{ $release }}/artisan view:clear
    php {{ $release }}/artisan cache:clear
    php {{ $release }}/artisan route:clear
    php {{ $release }}/artisan config:clear
    echo "Laravel Cache cleared"
@endtask

@task('deployment_finish', ['on' => 'web'])
    rm -rf {{ $path }}/repo
    mv {{ $release }}/.git {{ $path }}/repo
    ln -nfs {{ $release }} {{ $path }}/current
    echo "Deployment ({{ $date }}) finished"
@endtask

    
@task('restart_queue', ['on' => 'web']) 
    echo "Queue restarted"
    php {{ $release }}/artisan queue:restart --quiet
@endtask

@task('deployment_cleanup', ['on' => 'web'])
    cd {{ $path }}/releases
    find . -maxdepth 1 -name "20*" | sort | head -n -{{$deploy_keep}} | xargs rm -Rf
    echo "Cleaned up old deployments"
    rm -rf temp/*
    echo "Cleaned temporary files"
@endtask

@task('health_check', ['on' => 'web'])
    @if ( ! empty($healthCheckUrl) )
        if [ "$(curl -u @if(isset($httpUser) && !empty($httpUser)) "{{$httpUser}}" @endif --write-out "%{http_code}\n" --silent --output /dev/null {{ $healthCheckUrl }})" == "200" ]; then
            printf "\033[0;32mHealth check to {{ $healthCheckUrl }} OK\033[0m\n"
        else
            printf "\033[1;31mHealth check to {{ $healthCheckUrl }} FAILED\033[0m\n"
        fi
    @else
        echo "Pas de check du statut du site"
    @endif
@endtask

@task('optionnal_build_assets', ['on', 'localhost'])
    npm run twill-build
    npm run prod
@endtask

@task('create_uploads_folder', ['on' => 'web'])
    echo "create temporary folders"
    mkdir -p  {{$path .'/temp/public'}}  && chgrp www-data {{$path .'/temp/public'}} &&  chmod -R g+w {{$path .'/temp/public'}} 
    @if(sizeof($uploadables_folders)>0)
        @foreach($uploadables_folders as $uploadable) 
                echo "mkdir : {{$uploadable}}"
                mkdir -p {{$path .'/temp/' . $uploadable}} && chgrp www-data {{$path .'/temp/' . $uploadable}} && chmod -R g+w {{$path .'/temp/' . $uploadable}}
        @endforeach
    @endif
@endtask

@task('uploads_data', ['on' => 'localhost'])
    @if(sizeof($uploadables_folders)>0)
        @foreach($uploadables_folders as $uploadable)
            if [ -e "{{$uploadable}}" ]; then
                echo "uploading : {{$uploadable}}"
                scp -r $(pwd)/{{$uploadable}}/* {{$server}}:{{$path .'/temp/' . $uploadable}}
            fi
        @endforeach
    @endif
    @if(sizeof($uploadables_files)>0)
        @foreach($uploadables_files as $up_files)
            scp $(pwd)/{{$up_files}} {{$server}}:{{$path .'/temp/' . $up_files}}
        @endforeach
    @endif
@endtask

@task('uploads_apply', ['on' => 'web'])
    @foreach($uploadables_folders as $uploadable)
        if [ -e "{{$path .'/temp/' . $uploadable}}" ]; then
            echo "applying : {{$uploadable}}"
            rm -rf {{$release .'/' . $uploadable}}
            echo {{$path .'/temp/' . $uploadable }}
            echo {{$release .'/' . $uploadable}}
            mkdir {{$release .'/' . $uploadable}} && chgrp www-data {{$release .'/' . $uploadable}} && chmod -R g+w {{$release .'/' . $uploadable}}
            mv -v {{$path .'/temp/' . $uploadable . '/*' }} {{$release .'/' . $uploadable }}
            rm -rf {{$path .'/temp/' . $uploadable }}
        fi
    @endforeach

    @foreach($uploadables_files as $up_files)
        if [ -e "{{$path .'/temp/' . $up_files}}" ]; then
            echo "applying : {{$up_files}}"
            rm -rf {{$release .'/' . $up_files}}
            mv {{$path .'/temp/' . $up_files}} {{$release .'/' . $up_files}}
            rm -rf {{$path .'/temp/' . $up_files }}
        fi
    @endforeach
@endtask


@task('deployment_rollback', ['on' => 'web'])
    cd {{ $path }}
    ln -nfs {{ $path }}/releases/$(find . -maxdepth 1 -name "20*" | sort  | tail -n 2 | head -n1) {{ $path }}/current
    echo "Rolled back to $(find . -maxdepth 1 -name "20*" | sort  | tail -n 2 | head -n1)"
@endtask

{{-- Errors handling, syntax is php --}}
@error
    echo '/r task : '.   $task . ' error , check your configurations.';
    if($task == 'uploads_data') {
        shell_exec('rm - rf ../temp/*');
        exit;
    }
    if($task == 'deployement_migrate') {
        shell_exec('php current/artisan migrate:rollback');
        exit;
    }
@enderror

{{-- 
@finished    
    @slack($slackWebhook, '#deployments', "Deployment on {$server}: {$date} complete")    
@endfinished
--}}
